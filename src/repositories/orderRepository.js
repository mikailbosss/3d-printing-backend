import OrderModel from '../model/orderModel';

class OrderRepository {
  static order = async (order) => await OrderModel.create(order)
}

export default OrderRepository;
