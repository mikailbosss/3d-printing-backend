import UserModel from '../model/userModel';

class UserRepository {
  static createAccount = async (user) => {
    await UserModel.create(user);
  }

  static findUserByEmail = async (email) => UserModel.findOne({ email })
}

export default UserRepository;
