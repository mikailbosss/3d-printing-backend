import mongoose from 'mongoose';

const orderSchema = new mongoose.Schema({
  nameOrder: { type: String, required: true },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'Users' },
  url: { type: String, required: true },
  material: { type: String, required: true },
  notes: { type: String, required: true },
  color: { type: String, required: true },
  leadTime: { type: String, required: true },
  shipping: { type: String, required: true },
  shippingInformation: { type: String, required: true },
});

const orderModel = mongoose.model('Orders', orderSchema, 'Orders');

module.exports = orderModel;
