import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  username: { type: String, required: true },
  firstName: { type: String },
  lastName: { type: String },
  password: { type: String, required: true },
  email: { type: String, required: true },
  address: { type: String },
});

module.exports = mongoose.model('Users', userSchema, 'Users');
