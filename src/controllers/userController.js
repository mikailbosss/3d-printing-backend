import UserService from '../services/userService';

const { createAccount, login } = UserService;
class userController {
  static createAccount = async (req, res, next) => {
    console.log(req.body);
    try {
      const newUser = await createAccount(req.body);
      res.status(200).json(newUser);
    } catch (e) {
      next(e);
    }
  }

  static login =async (req, res, next) => {
    const { email, password } = req.body;
    try {
      const user = await login(email, password);
      console.log(user);
      res.status(200).json(user);
    } catch (e) {
      next(e);
    }
  }
}

export default userController;
