import OrderService from '../services/orderService';

const { order } = OrderService;
class OrderController {
  static order = async (req, res, next) => {
    try {
      const url = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
      const newOrder = await order(req.body, url);
      return res.status(200).json(newOrder);
    } catch (e) {
      next(e);
    }
  }
}

export default OrderController;
