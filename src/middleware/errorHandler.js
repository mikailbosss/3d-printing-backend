const errorHandler = (err, req, res, next) => {
  res.status(err.status || 500);
  return res.json({
    code: err.status || 500,
    message: err.message || 'Internal server error',
  });
};

module.exports = errorHandler;
