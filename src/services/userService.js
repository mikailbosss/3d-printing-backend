import Bcrypt from 'bcrypt';
import { isEmpty } from 'lodash';
import UserRepository from '../repositories/userRepository';
import { createError } from '../utils/errorHandler';

const { createAccount, findUserByEmail } = UserRepository;
class UserService {
  static createAccount = async (newUser) => {
    const user = await findUserByEmail(newUser.email);
    if (!isEmpty(user)) {
      throw createError(404, 'email is already exist');
    }
    const password = await Bcrypt.hash(newUser.password, 10);
    const userWithHashPassword = { ...newUser, password };
    const createdUser = await createAccount(userWithHashPassword);
    return createdUser;
  }

  static login = async (email, password) => {
    const user = await findUserByEmail(email);
    if (isEmpty(user)) {
      throw createError(401, 'user is not exist');
    }
    const isMatch = await Bcrypt.compare(password, user.password);
    if (isMatch) {
      throw createError(401, 'password is not correct!');
    }
    const userData = {
      username: user.username, firstname: user.firstName, lastname: user.lastName, address: user.address,
    };
    return user;
  }
}

export default UserService;
