import OrderRepository from '../repositories/orderRepository';

const { order } = OrderRepository;
class OrderService {
  static order = async (newOrder, url) => {
    const newOrderWithUrl = { ...newOrder, url };
    return await order(newOrderWithUrl);
  }
}

export default OrderService;
