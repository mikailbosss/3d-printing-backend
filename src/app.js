import cors from 'cors';
import express from 'express';
import logger from 'morgan';
import http from 'http';
import mongoose from 'mongoose';
import routes from './routes';
import ErrorHandler from './middleware/errorHandler';

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

mongoose.connect('mongodb://localhost:27017/3DPrinting', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

if (mongoose.connection) {
  console.log('success connecting to db');
}

routes(app);
app.use(ErrorHandler);
const port = 8080;
const server = http.createServer(app);

server.listen(port);
