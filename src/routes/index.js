import express from 'express';
import userRouter from './userRoute';
import orderRoute from './orderRoute';

const router = express.Router();

const routes = (app) => {
  app.use('/', router);
  router.use('/user', userRouter);
  router.use('/order', orderRoute);
};

module.exports = routes;
