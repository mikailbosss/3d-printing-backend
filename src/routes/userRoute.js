import express from 'express';
import userController from '../controllers/userController';

const userRouter = express.Router();
const { createAccount, login } = userController;

userRouter.post('/login', login);
userRouter.post('/', createAccount);

export default userRouter;
