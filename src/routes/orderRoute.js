import express from 'express';
import OrderController from '../controllers/orderController';
import Multer from '../middleware/multer';

const orderRoute = express.Router();
const { order } = OrderController;

orderRoute.post('/', Multer.single('stl'), order);

export default orderRoute;
